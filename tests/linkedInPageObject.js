module.exports = {
    'Sign in to LinkedIn' : function (browser) {
      browser.maximizeWindow();  
      var signInPage = browser.page.linkedInSignIn();
      signInPage.navigate();

      signInPage.expect.element('@emailTextBox').to.be.enabled;
      signInPage.setValue('@emailTextBox', 'ntthaonhi1489@gmail.com');

      signInPage.expect.element('@passwordTextBox').to.be.enabled; 
      signInPage.setValue('@passwordTextBox', '123456nhi');
      signInPage.signIn();
  
      var homePage = browser.page.linkedInHome();
      homePage.clickProfileName();
  
      var profilePage = browser.page.linkedInProfile();
      profilePage.expect.element('@profileName').to.contain.text('nhi nguyen');
      profilePage.expect.element('@profileTitle').to.contain.text('Developer at Home');
  
      browser.end();
    }
  };