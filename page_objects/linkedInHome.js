const goToProfileCommands = {
  clickProfileName() {
    this.waitForElementVisible('@profileName', 1000)
      .click('@profileName')
      .api.pause(1000);

    return this; 
  }
};
  
  module.exports = {
    commands: [goToProfileCommands],
    elements: {
      profileName: {selector: 'a[data-control-name="identity_welcome_message"]'}
    }
  };