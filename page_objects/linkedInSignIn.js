const signInCommands = {
    signIn() {
      this.waitForElementVisible('@signInButton', 1000)
        .click('@signInButton')
        .api.pause(1000);
  
      return this; 
    }
  };

module.exports = {
    url: 'https://www.linkedin.com/',
    commands: [signInCommands],
    elements: {
      emailTextBox: {selector: 'input[name=session_key]'},
      passwordTextBox: {selector: 'input[name="session_password"]'},
      signInButton: {selector: 'input[value="Sign in"]'}
    }
  };