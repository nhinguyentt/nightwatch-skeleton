module.exports = {
    elements: {
      profileName: {selector: 'h1[class="pv-top-card-section__name inline t-24 t-black t-normal"]'},
      profileTitle: {selector: 'h2[class="pv-top-card-section__headline mt1 t-18 t-black t-normal"]'}
    }
  };